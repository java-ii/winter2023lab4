public class Student{ 
  
  private String name; // PART 3 Question 4
  private int height; 
  private String hairType; 
  private String eyeColor; 
  private String favFood; 
  private String favAnimal; 
  private String favDrink;
  public int amountLearnt;  //Lab 4 PART 1 Question 1
  
  public Student(String name, int height, String hairType, String eyeColor, String favFood, String favDrink){ //Lab 4-2 question 1
  //this.name="Noname";   //Lab 4-2 Question 1, Question 3
    this.name=name;
  //this.height=0;        //Lab 4-2 Question 1,  Question 3
    this.height=height;
  //this.hairType="NoHairType";  //Lab 4-2 Question 1,  Question 3
    this.hairType=hairType;
  //this.eyeColor="NoEyeColor";  //Lab 4-2 Question 1,  Question 3
    this.eyeColor=eyeColor;
  //this.favFood="NoFavFood";    //Lab 4-2 Question 1,  Question 3 
    this.favFood=favFood;
  //this.favDrink="NoFavDrink";//Lab 4-2 Question 1,  Question 3
    this.favDrink=favDrink;
  this.favAnimal="NoFavAnimal";  //Lab 4-2 Question 1,  Question 3
  this.amountLearnt=0;         //Lab 4-2 Question 1,  Question 3
  }
  
  public void bioInfo(){
    
    System.out.println(name + " Bio -->"); 
    System.out.println("Height: " + height + "cm"); 
    System.out.println("Hairtype: " + hairType);
    System.out.println("Eyecolor: " + eyeColor);
    
  } 
  public void favStuff(){  
    
    System.out.println(name + "'s Favorite stuff -->"); 
    System.out.println("Favorite Food: " + favFood); 
    System.out.println("Favorite animal: " + favAnimal); 
    System.out.println("Favorite drink: " + favDrink);  
  }
  public void learn(int amountStudied){ // Lab 4 (PART 1 Question 4) PART 2 Question 1) 
    // amountLearnt = amountLearnt + 1; // PART 1 Question 4
 // amountLearnt = amountLearnt + amountStudied; // PART 2 Question 1
    if(amountStudied > 0){   //PART 2.5 Question 1
      amountLearnt = amountLearnt + amountStudied;  // PART 2.5 Question 1
    }
  }
  public String getName(){  // PART 3 Question 1
    return this.name;
  } 
  public int getHeight(){    // PART 3 Question 1
    return this.height;
  }
  public String getHairType(){   // PART 3 Question 1
    return this.hairType;
  }
  public String getEyeColor(){   // PART 3 Question 1
    return this.eyeColor;
  } 
  public String getFavFood(){    // PART 3 Question 1
    return this.favFood; 
  } 
  public String getFavDrink(){   // PART 3 Question 1
    return this.favDrink; 
  } 
  public String getFavAnimal(){  // PART 3 Question 1
    return this.favAnimal; 
  }
  public int getAmountLearnt(){  // PART 3 Question 1
    return this.amountLearnt;
  }
  /*
  public void setName(String newName){  // PART 3 Question 5, Question 12
 //name = newName; //PART 3 Question 9
 //name = name;    //PART 3 Question 10 
 //this.name = name; //PART 3 Question 11
    this.name = newName; //PART 3 Question 13
  } 
  public void setHeight(int newHeight){
    this.height = newHeight;
  }
  public void setHairType(String newHairType){ 
    this.hairType = newHairType; 
  }
  public void setEyeColor(String newEyeColor){ 
    this.eyeColor = newEyeColor; 
  } 
  public void setFavFood(String newFavFood){ 
    this.favFood = newFavFood; 
  } 
   public void setFavDrink(String newFavDrink){
    this.favDrink = newFavDrink;
  }
  */
  public void setFavAnimal(String newFavAnimal){ 
    this.favAnimal = newFavAnimal;
  }
 
}
