import java.util.Random;
import java.util.Scanner;
 public class RiceCooker{ 
   
   private String name; //the series of the cooker           //PART 2 Question 1
   private String typeFood; //fish, rice, chicken, etc.      //PART 2 Question 1
   private int amountWater; //in units of cups               //PART 2 Question 1
   int timer; //in units of minutes
   int quantity; //in units of grams
   int temperature; //in units of celsius 
   
   public void setUp(){ //sets the parameters time temperature
  System.out.println(name + " is ready to be used!"); 
  System.out.println("Today we are going to be cooking " + typeFood); 
  System.out.println("Preping the Ingredients! Chop! Chop!"); 
  System.out.println("Putting in " + amountWater + " cups of water into " + name + "!"); 
  System.out.println("Putting in " + quantity + " grams of " + typeFood + " and the ingredients into " + name + "!"); 
  System.out.println("Setting the temperature to " + temperature + " Celsius!");
  System.out.println("Setting the time to " + timer + " minutes!");   
   }
   public void cookingTime(){ // sets the parameters for type, amount and quantity
  Scanner reader = new Scanner(System.in);
  System.out.println("Say GET COOKING to start the process!"); 
  System.out.print("Voice: ");
  String voice = reader.next();
  System.out.println("Sweet muffins! Lets get Cooking!");
  Random coinFlip = new Random();      
  boolean heads = true;
  for(int timeLapse = 5; timer>=0;){ 
    boolean flippedCoin = coinFlip.nextBoolean();
    if(flippedCoin != heads){
   System.out.println();
   System.out.println("BEEP! BOOP! BEEP! BOOP!"); 
   System.out.println("COOKING TIME REMAINING: " + timer);
   timer = timer - timeLapse;
    }
    else if(flippedCoin = heads){ 
   System.out.println();
   System.out.println("COOKING! COOKING! COOKING!"); 
   System.out.println("COOKING TIME REMAINING: " + timer);
   timer = timer - timeLapse;
    }
  }   
   }  
   
 /* LAB 4 */
 
  // This is new INSTANCE METHOD with input values
   public void unFrost(String typeFood, int timer, int temperature){ //PART 2 Question 1
  
  helper(typeFood, timer, temperature); 
  System.out.println(typeFood + " needs to be deFrosted!"); 
  System.out.println("Setting time of " + timer + " minutes!"); 
  System.out.println("The temperature should be " + temperature + "c*"); 
  System.out.println("Unfrosting...."); 
  System.out.println("Done!");
   }
   
   //This is HELPER METHOD validating variables values
   private void helper(String typeFood, int timer, int temperature){ //PART 2 Question 2, Question 3, Question 4
  if(typeFood.length() > 0){
    this.typeFood = typeFood; 
  }
  if(timer > 0){
    this.timer = timer; 
  }
  if(temperature > 0){
    this.temperature = temperature;
  }    
   }
   
   
   //This are the GETTERS
    public String getName(){    // PART 2 Question 2
  return this.name;
   }
  public String getTypeFood(){    // PART 2 Question 2
  return this.typeFood;
   }
   public int getAmountWater(){    // PART 2 Question 2
  return this.amountWater;
   }
   
   
   //This are the SETTERS
   
   
   public void setName(String newName){ // PART 2 Question 2, PART 3 QUESTION 2
   this.name = newName;
   }
   
   public void setTypeFood(String newTypeFood){ // PART 2 Question 2
  this.typeFood = newTypeFood;
   }
   public void setAmountWater(int newAmountWater){ // PART 2 Question 2
  this.amountWater = newAmountWater;
   } 
   
   
  //This is the CONSTRUCTOR
   public RiceCooker(String name, String typeFood, int amountWater){   //PART 3 Question 1
   
   this.name = "NoName";                                               
   this.typeFood = "NoType";                                           
   this.amountWater = 0;                                          
   }  
   }
   