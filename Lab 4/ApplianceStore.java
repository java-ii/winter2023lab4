import java.util.Scanner;
public class ApplianceStore{ 
  
  public static void main(String[] args){
    
    Scanner reader = new Scanner(System.in);
    RiceCooker[] riceCookers = new RiceCooker[4];
    System.out.println();
    System.out.println("Welcome to the ApplianceStore Simulator!");
    System.out.println();
    System.out.println("Set the parameters to start the simulation!");
    System.out.println(); 
    for(int i=0; i<riceCookers.length; i++){ 
      System.out.println();
      System.out.print("RiceCooker name: ");                          //PART 3 Question 2
      String name = reader.next();                                    //PART 3 Question 2
      System.out.print("Food type: ");                                //PART 3 Question 2
      String foodType = reader.next();                                //PART 3 Question 2
      System.out.print("Amount of water(c): ");                       //PART 3 Question 2
      int amountWater = reader.nextInt();                             //PART 3 Question 2
      riceCookers[i] = new RiceCooker(name, foodType, amountWater);    //PART 3 Question 2
      /*
       System.out.print("RiceCooker name: ");
       //riceCookers[i].name = reader.next();              // PART 2 Question 2
       String name = reader.next(); 
       riceCookers[i].RiceCooker(name);
       //riceCookers[i].setName(name);                     // PART 3 Question 2
       System.out.print("Food type: ");
       //riceCookers[i].typeFood = reader.next();           //PART 2 Question 2
       String typeFood = reader.next();
       riceCookers[i].setTypeFood(typeFood);
       System.out.print("Amount of water(c): "); 
       //riceCookers[i].amountWater = reader.nextInt();     //PART 2 Question 2
       int amountWater = reader.nextInt();
       riceCookers[i].setAmountWater(amountWater);
       */
      System.out.print("Time(min): ");
      riceCookers[i].timer = reader.nextInt();
      System.out.print("Quantity(g): "); 
      riceCookers[i].quantity = reader.nextInt(); 
      System.out.print("Temperature(c): ");  
      riceCookers[i].temperature = reader.nextInt();
    }
    System.out.println();
    System.out.println("Current name: " + riceCookers[3].getName());
    System.out.println("Current type of food: " + riceCookers[3].getTypeFood());
    System.out.println("Current amount of water: " + riceCookers[3].getAmountWater());
    System.out.println();
    System.out.print("Enter new RiceCooker name: ");  
    String newName = reader.next();
    riceCookers[3].setName(newName);
    System.out.print("Enter new  type of Food: ");
    String newTypeFood = reader.next();
    riceCookers[3].setTypeFood(newTypeFood);
    System.out.print("Enter new water amount: ");
    int newAmountWater = reader.nextInt();
    riceCookers[3].setAmountWater(newAmountWater);
    System.out.println();
    System.out.println("New name: " + riceCookers[3].getName());
    System.out.println("New food type: " + riceCookers[3].getTypeFood());
    System.out.println("New amount of water: " + riceCookers[3].getAmountWater());
    System.out.println();
    System.out.println("This is appliance number 4 parameters");
    System.out.println();
    System.out.println("1: " + riceCookers[3].getName());
    System.out.println("2: " + riceCookers[3].getTypeFood());
    System.out.println("3: " + riceCookers[3].quantity);
    System.out.println("4: " + riceCookers[3].timer);
    System.out.println("5: " + riceCookers[3].temperature); 
    System.out.println();
    riceCookers[0].setUp();
    System.out.println();
    riceCookers[0].cookingTime(); 
    System.out.println(); 
    System.out.println("Thank you for participating in ApplianceStore Simulator!"); 
    System.out.println("Until next time!");
    
    //Lab 4 Graded 
    System.out.println();
    System.out.println("Lab 4");
    String food ="Chicken";   //PART 1 Question 5, Question 6
    int timer = 30; 
    int temperature = 650;
    riceCookers[1].unFrost(food, timer, temperature); //PART 1 Question 5, Question 6
  }
}